# SPDX-FileCopyrightText: 2022, 2023 Mincho Kondarev <mkondarev@yahoo.de>
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-30 00:38+0000\n"
"PO-Revision-Date: 2023-11-22 11:37+0100\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: C <kde-i18n-doc@kde.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.08.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: declarativeimports/plasmaextracomponents/qml/BasicPlasmoidHeading.qml:81
#, kde-format
msgid "More actions"
msgstr "Повече действия"

#: declarativeimports/plasmaextracomponents/qml/ExpandableListItem.qml:516
#, kde-format
msgctxt "@action:button"
msgid "Collapse"
msgstr "Свиване"

#: declarativeimports/plasmaextracomponents/qml/ExpandableListItem.qml:516
#, kde-format
msgctxt "@action:button"
msgid "Expand"
msgstr "Разгъване"

#: declarativeimports/plasmaextracomponents/qml/PasswordField.qml:49
#, kde-format
msgid "Password"
msgstr "Парола"

#: declarativeimports/plasmaextracomponents/qml/SearchField.qml:60
#, kde-format
msgid "Search…"
msgstr "Търсене..."

#: declarativeimports/plasmaextracomponents/qml/SearchField.qml:62
#, kde-format
msgid "Search"
msgstr "Търсене"

#: declarativeimports/plasmaextracomponents/qml/SearchField.qml:73
#, kde-format
msgid "Clear search"
msgstr "Изчистване на търсенето"

#: plasma/applet.cpp:300
#, kde-format
msgid "Unknown"
msgstr "Неизвестно"

#: plasma/applet.cpp:743
#, kde-format
msgid "Activate %1 Widget"
msgstr "Активиране на %1 Widget"

#: plasma/containment.cpp:97 plasma/private/applet_p.cpp:108
#, kde-format
msgctxt "%1 is the name of the applet"
msgid "Remove %1"
msgstr "Премахване на %1"

#: plasma/containment.cpp:103 plasma/corona.cpp:367 plasma/corona.cpp:485
#, kde-format
msgid "Enter Edit Mode"
msgstr "Влизане в режим на редактиране"

#: plasma/containment.cpp:106 plasma/private/applet_p.cpp:113
#, kde-format
msgctxt "%1 is the name of the applet"
msgid "Configure %1..."
msgstr "Конфигуриране на %1..."

#: plasma/corona.cpp:316 plasma/corona.cpp:470
#, kde-format
msgid "Lock Widgets"
msgstr "Заключване на уиджети"

#: plasma/corona.cpp:316
#, kde-format
msgid "Unlock Widgets"
msgstr "Отключване на уиджети"

#: plasma/corona.cpp:365
#, kde-format
msgid "Exit Edit Mode"
msgstr "Излизане от режима за редактиране"

#. i18n: ectx: label, entry, group (CachePolicies)
#: plasma/data/kconfigxt/libplasma-theme-global.kcfg:9
#, kde-format
msgid "Whether or not to create an on-disk cache for the theme."
msgstr "Създаване на кеш на диска за темата."

#. i18n: ectx: label, entry, group (CachePolicies)
#: plasma/data/kconfigxt/libplasma-theme-global.kcfg:14
#, kde-format
msgid ""
"The maximum size of the on-disk Theme cache in kilobytes. Note that these "
"files are sparse files, so the maximum size may not be used. Setting a "
"larger size is therefore often quite safe."
msgstr ""
"Максималният размер на кеша на темата на диска в килобайта. Имайте предвид, "
"че тези файлове са малки файлове, така че максималният размер може да не се "
"използва. Следователно задаване на по-голям размер често е безопасно."

#: plasma/private/applet_p.cpp:138
#, kde-format
msgctxt "Package file, name of the widget"
msgid "Could not open the %1 package required for the %2 widget."
msgstr "Неуспешно отваряне на пакета %1, необходим за уиджета %2."

#: plasma/private/applet_p.cpp:146
#, kde-format
msgid "Show Alternatives..."
msgstr "Показване на алтернативи..."

#: plasma/private/applet_p.cpp:253
#, kde-format
msgid "Widget Removed"
msgstr "Уиджетът е премахнат"

#: plasma/private/applet_p.cpp:254
#, kde-format
msgid "The widget \"%1\" has been removed."
msgstr "Уиджетът \"%1 \" е премахнат."

#: plasma/private/applet_p.cpp:258
#, kde-format
msgid "Panel Removed"
msgstr "Панелът е премахнат"

#: plasma/private/applet_p.cpp:259
#, kde-format
msgid "A panel has been removed."
msgstr "Панелът е премахнат."

#: plasma/private/applet_p.cpp:262
#, kde-format
msgid "Desktop Removed"
msgstr "Работният плот е премахнат"

#: plasma/private/applet_p.cpp:263
#, kde-format
msgid "A desktop has been removed."
msgstr "Работният плот е премахнат."

#: plasma/private/applet_p.cpp:266
#, kde-format
msgid "Undo"
msgstr "Отмяна"

#: plasma/private/applet_p.cpp:357
#, kde-format
msgid "Widget Settings"
msgstr "Настройки на уиджет"

#: plasma/private/applet_p.cpp:364
#, kde-format
msgid "Remove this Widget"
msgstr "Премахване на този уиджет"

#: plasma/private/containment_p.cpp:58
#, kde-format
msgid "Remove this Panel"
msgstr "Премахване на този панел"

#: plasma/private/containment_p.cpp:60
#, kde-format
msgid "Remove this Activity"
msgstr "Премахване на тази дейност"

#: plasma/private/containment_p.cpp:66
#, kde-format
msgid "Activity Settings"
msgstr "Настройки на дейност"

#: plasma/private/containment_p.cpp:78
#, kde-format
msgid "Add Widgets..."
msgstr "Добавяне на уиджети..."

#: plasma/private/containment_p.cpp:197
#, kde-format
msgid "Could not find requested component: %1"
msgstr "Не можа да се намери заявения компонент:%1"

#. i18n: ectx: property (text), widget (QLabel, label)
#: plasma/private/publish.ui:17
#, kde-format
msgid ""
"Sharing a widget on the network allows you to access this widget from "
"another computer as a remote control."
msgstr ""
"Споделянето на уиджета в мрежата ви позволява да получите достъп до него от "
"друг компютър като отдалечено управление."

#. i18n: ectx: property (text), widget (QCheckBox, publishCheckbox)
#: plasma/private/publish.ui:27
#, kde-format
msgid "Share this widget on the network"
msgstr "Споделяне на този уиджет в мрежата"

#. i18n: ectx: property (text), widget (QCheckBox, allUsersCheckbox)
#: plasma/private/publish.ui:37
#, kde-format
msgid "Allow everybody to freely access this widget"
msgstr "Позволяване на всички да имат свободен достъп до този уиджет"

#: plasma/private/service_p.h:32
#, kde-format
msgctxt "Error message, tried to start an invalid service"
msgid "Invalid (null) service, can not perform any operations."
msgstr "Невалидна (нулева) услуга, не може да извършва никакви операции."

#: plasmaquick/appletquickitem.cpp:519
#, kde-format
msgid "The root item of %1 must be of type ContainmentItem"
msgstr "Основният елемент на  %1 трябва да бъде от тип ContaimentItem"

#: plasmaquick/appletquickitem.cpp:524
#, kde-format
msgid "The root item of %1 must be of type PlasmoidItem"
msgstr "Основният елемент на  %1 трябва да бъде от тип PlasmoidItem"

#: plasmaquick/appletquickitem.cpp:531
#, kde-format
msgid "Unknown Applet"
msgstr "Неизвестна приставка"

#: plasmaquick/appletquickitem.cpp:544
#, kde-format
msgid ""
"This Widget was written for an unknown older version of Plasma and is not "
"compatible with Plasma %1. Please contact the widget's author for an updated "
"version."
msgstr ""
"Този уиджет е написан за неизвестна по-стара версия на Plasma и не е "
"съвместим с Plasma %1. Моля, свържете се с автора на уиджета за по-"
"актуализирана версия."

#: plasmaquick/appletquickitem.cpp:550
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with Plasma %2. "
"Please contact the widget's author for an updated version."
msgstr ""
"Този уиджет е написан за Plasma %1 и не е съвместим с Plasma %2. Моля, "
"свържете се с автора на уиджета за по-актуализирана версия."

#: plasmaquick/appletquickitem.cpp:555
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with Plasma %2. "
"Please update Plasma in order to use the widget."
msgstr ""
"Този уиджет е написан за Plasma %1 и не е съвместим с Plasma %2. Моля, "
"актуализирайте Plasma, за да можете да го използвате."

#: plasmaquick/appletquickitem.cpp:574
#, kde-format
msgid "Error loading QML file: %1 %2"
msgstr "Грешка при зареждането на QML файл: %1 %2"

#: plasmaquick/appletquickitem.cpp:577
#, kde-format
msgid "Error loading Applet: package does not exist. %1"
msgstr "Грешка при зареждането на аплета: пакетът не съществува. %1"

#: plasmaquick/configview.cpp:104
#, kde-format
msgid "%1 Settings"
msgstr "%1 Настройки"

#: plasmaquick/plasmoid/containmentitem.cpp:536
#, kde-format
msgid "Plasma Package"
msgstr "Пакет на Plasma"

#: plasmaquick/plasmoid/containmentitem.cpp:540
#, kde-format
msgid "Install"
msgstr "Инсталиране"

#: plasmaquick/plasmoid/containmentitem.cpp:551
#, kde-format
msgid "Package Installation Failed"
msgstr "Инсталацията на пакета е неуспешна"

#: plasmaquick/plasmoid/containmentitem.cpp:567
#, kde-format
msgid "The package you just dropped is invalid."
msgstr "Пакетът, който току-що пуснахте, е невалиден."

#: plasmaquick/plasmoid/containmentitem.cpp:576
#: plasmaquick/plasmoid/containmentitem.cpp:645
#, kde-format
msgid "Widgets"
msgstr "Уиджети"

#: plasmaquick/plasmoid/containmentitem.cpp:581
#, kde-format
msgctxt "Add widget"
msgid "Add %1"
msgstr "Добавяне %1"

#: plasmaquick/plasmoid/containmentitem.cpp:595
#: plasmaquick/plasmoid/containmentitem.cpp:649
#, kde-format
msgctxt "Add icon widget"
msgid "Add Icon"
msgstr "Добавяне на икона"

#: plasmaquick/plasmoid/containmentitem.cpp:607
#, kde-format
msgid "Wallpaper"
msgstr "Тапет"

#: plasmaquick/plasmoid/containmentitem.cpp:617
#, kde-format
msgctxt "Set wallpaper"
msgid "Set %1"
msgstr "Задаване на %1"

#: plasmaquick/plasmoid/dropmenu.cpp:28
#, kde-format
msgid "Content dropped"
msgstr "Съдържанието е пуснато"
